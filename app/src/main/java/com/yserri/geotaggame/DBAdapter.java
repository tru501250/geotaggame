package com.yserri.geotaggame;

/**
 * Created by YserriCom4980 on 3/10/2016.  testing
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DBAdapter {
    // DB Fields
    public static final String KEY_ROWID = "_id";
    public static final int COL_ROWID = 0;
    /*
     * CHANGE 1:
     */
    // TODO: Setup your fields here:
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESC = "desc";
    public static final String KEY_DATE = "date";
    public static final String KEY_ACTION = "action";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_TIME = "time";
    public static final String KEY_SCORE = "score";
    // TODO: Setup your field numbers here (0 = KEY_ROWID, 1=...)
    public static final int COL_TITLE = 1;
    public static final int COL_DESC = 2;
    public static final int COL_DATE = 3;
    public static final int COL_ACTION = 4;
    public static final int COL_LATITUDE = 5;
    public static final int COL_LONGITUDE = 6;
    public static final int COL_TIME = 6;
    public static final int COL_SCORE = 7;
    public static final String[] ALL_KEYS = new String[]{KEY_ROWID, KEY_TITLE, KEY_DESC,
            KEY_DATE, KEY_ACTION, KEY_LATITUDE, KEY_LONGITUDE, KEY_TIME, KEY_SCORE};
    // DB info: it's name, and the table we are using (just one).
    public static final String DATABASE_NAME = "MyDb";
    public static final String DATABASE_TABLE = "table3";
    // Track DB version if a new version of your app changes the format.
    public static final int DATABASE_VERSION = 6;
    /////////////////////////////////////////////////////////////////////
    //	Constants & Data
    /////////////////////////////////////////////////////////////////////
    // For logging:
    private static final String TAG = "DBAdapter";
    private static final String DATABASE_CREATE_SQL =
            "create table " + DATABASE_TABLE
                    + " (" + KEY_ROWID + " integer primary key autoincrement, "

			/*
             * CHANGE 2:
			 */
                    // TODO: Place your fields here!
                    // + KEY_{...} + " {type} not null"
                    //	- Key is the column name you created above.
                    //	- {type} is one of: text, integer, real, blob
                    //		(http://www.sqlite.org/datatype3.html)
                    //  - "not null" means it is a required field (must be given a value).
                    // NOTE: All must be comma separated (end of line!) Last one must have NO comma!!
                    + KEY_TITLE + " string not null, "
                    + KEY_DESC + " string not null,"
                    + KEY_DATE + " date not null,"
                    + KEY_ACTION + " string not null,"
                    + KEY_LATITUDE + " string not null,"
                    + KEY_LONGITUDE + " string not null,"
                    + KEY_TIME + " date not null,"
                    + KEY_SCORE + " number not null"

                    // Rest  of creation:
                    + ");";
    // Context of application who uses us.
    private final Context context;
    //for date
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String currentDateandTime = sdf.format(new Date());
    private DatabaseHelper myDBHelper;
    private SQLiteDatabase db;

    /////////////////////////////////////////////////////////////////////
    //	Public methods:
    /////////////////////////////////////////////////////////////////////

    public DBAdapter(Context ctx) {

        this.context = ctx;
        //context.deleteDatabase(DATABASE_NAME);
        myDBHelper = new DatabaseHelper(context);
    }

    // Open the database connection.
    public DBAdapter open() {

        db = myDBHelper.getWritableDatabase();
        return this;
    }

    // Close the database connection.
    public void close() {
        myDBHelper.close();
    }

    public void nuke() {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
    }

    // Add a new set of values to the database.
    public long insertRow(String title, String desc, String Date, String action, String latitude,
                          String longitude, String time, double score) {
		/*
		 * CHANGE 3:
		 */
        // TODO: Update data in the row with new fields.
        // TODO: Also change the function's arguments to be what you need!
        // Create row's data:
        Log.d("y", latitude + " " + longitude);
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TITLE, title);
        initialValues.put(KEY_DESC, desc);
        initialValues.put(KEY_DATE, Date);
        initialValues.put(KEY_ACTION, action);
        initialValues.put(KEY_LATITUDE, latitude);
        initialValues.put(KEY_LONGITUDE, longitude);
        initialValues.put(KEY_TIME, time);
        initialValues.put(KEY_SCORE, score);

        //context.deleteDatabase(DATABASE_NAME);
        // Insert it into the database.
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    // Delete a row from the database, by rowId (primary key)
    public boolean deleteRow(long rowId) {
        String where = KEY_ROWID + "=" + rowId;
        return db.delete(DATABASE_TABLE, where, null) != 0;
    }

    public void deleteAll() {
        Cursor c = getAllRows();
        long rowId = c.getColumnIndexOrThrow(KEY_ROWID);
        if (c.moveToFirst()) {
            do {
                deleteRow(c.getLong((int) rowId));
            } while (c.moveToNext());
        }
        c.close();
    }

    // Return all data in the database.
    public Cursor getAllRows() {
        String where = null;
        Cursor c = db.query(true, DATABASE_TABLE, ALL_KEYS,
                where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Get a specific row (by rowId)
    public Cursor getRow(long rowId) {
        String where = KEY_ROWID + "=" + rowId;
        Cursor c = db.query(true, DATABASE_TABLE, ALL_KEYS,
                where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Change an existing row to be equal to new data.
    public boolean updateRow(long rowId, String title, String desc, String date, String action) {
        String where = KEY_ROWID + "=" + rowId;

		/*
		 * CHANGE 4:
		 */
        // TODO: Update data in the row with new fields.
        // TODO: Also change the function's arguments to be what you need!
        // Create row's data:
        ContentValues newValues = new ContentValues();
        newValues.put(KEY_TITLE, title);
        newValues.put(KEY_DESC, desc);
        newValues.put(KEY_DATE, date);
        newValues.put(KEY_ACTION, action);

        // Insert it into the database.
        return db.update(DATABASE_TABLE, newValues, where, null) != 0;
    }


    /////////////////////////////////////////////////////////////////////
    //	Private Helper Classes:
    /////////////////////////////////////////////////////////////////////

    /**
     * Private class which handles database creation and upgrading.
     * Used to handle low-level database access.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase _db) {
            //_db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            _db.execSQL(DATABASE_CREATE_SQL);
            Log.d("here in db creation", "");
        }

        @Override
        public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading application's database from version " + oldVersion
                    + " to " + newVersion + ", which will destroy all old data!");

            // Destroy old database:
            _db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);

            // Recreate new database:
            onCreate(_db);
        }

    }
}