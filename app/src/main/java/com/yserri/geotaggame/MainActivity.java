package com.yserri.geotaggame;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int ERROR_DIALOG_REQUEST = 9001;
    GoogleMap mMap;
    DBAdapter myDb;
    Context ctx;
    double[] latitudeArr;
    double[] longitudeArr;
    private CircleOptions circleOptions0;
    private CircleOptions circleOptions1;
    private CircleOptions circleOptions2;
    LatLng point0;
    LatLng point1;
    LatLng point2;
    int score;
    boolean flag0, flag1, flag2;
    float distance0;
    float distance1;
    float distance2;
    /*Location currentLocation;
    double latCurrent;
    double lngCurrent;
    LatLng latLngCurrent;*/
    float smallest;
    float distanceFrom;
    Marker marker;
    Circle shape;
    float smallestDistance;
    private GoogleApiClient mLocationClient;
    private LocationListener mListener;
    private static final float DEFAULTZOOM = 15;
    static Date cDate = new Date();
    static String DATE = new SimpleDateFormat("yyyy-MM-dd").format(cDate);
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = this;
        openDB();
        latitudeArr = new double[3];
        longitudeArr = new double[3];

        Cursor cursor = myDb.getAllRows();
        displayRecordSet(cursor);

        readJSON readOBJ = new readJSON(ctx);
        readOBJ.execute();

        if (servicesOK()) {
            setContentView(R.layout.activity_map);
            if (initMap()) {
                mLocationClient = new GoogleApiClient.Builder(this)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();
                mLocationClient.connect();
                mMap.setMyLocationEnabled(true);
            } else {
                Toast.makeText(this, "Map not connected!", Toast.LENGTH_SHORT).show();
            }
        } else {
            setContentView(R.layout.activity_main);
        }
        setPoint();
        infowin();
        point();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.mapTypeNone:
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
            case R.id.mapTypeNormal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.mapTypeSatellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.mapTypeTerrain:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.mapTypeHybrid:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean servicesOK() {
        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
            Dialog dialog =
                    GooglePlayServicesUtil.getErrorDialog(isAvailable, this, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "Can't connect to mapping service", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private boolean initMap() {
        if (mMap == null) {
            SupportMapFragment mapFragment =
                    (SupportMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.map);
            mMap = mapFragment.getMap();
        }
        return (mMap != null);
    }

    public void onClick_DisplayRecords(View v) {
        Cursor cursor = myDb.getAllRows();
        displayRecordSet(cursor);
    }

    private void displayRecordSet(Cursor cursor) {
        String message = "";
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(DBAdapter.COL_ROWID);
                String title = cursor.getString(DBAdapter.COL_TITLE);
                String desc = cursor.getString(DBAdapter.COL_DESC);
                String date = cursor.getString(DBAdapter.COL_DATE);
                String action = cursor.getString(DBAdapter.COL_ACTION);
                String latitude = cursor.getString(DBAdapter.COL_LATITUDE);
                String longitude = cursor.getString(DBAdapter.COL_LONGITUDE);
                String time = cursor.getString(DBAdapter.COL_TIME);
                String score = cursor.getString(DBAdapter.COL_SCORE);
                message += "id=" + id
                        + ", title=" + title
                        + ", desc=" + desc
                        + ", date=" + date
                        + ", action=" + action
                        + ", latitude=" + latitude
                        + ", longitude=" + longitude
                        + ", time=" + time
                        + ", score=" + score
                        + "\n";
                if (desc.equals("Center")) {
                    latitudeArr[count] = Double.valueOf(latitude);
                    longitudeArr[count] = Double.valueOf(longitude);
                    Log.d("array " + count, latitudeArr[count] + " " + longitudeArr[count]);
                    count++;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        Log.d("TESTing db", message);
    }

    private void openDB() {
        myDb = new DBAdapter(ctx);
        myDb.open();
    }

    private void closeDB() {
        myDb.close();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        closeDB();
    }

    private void gotoLocation(double lat, double lng, float zoom) {
        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        mMap.moveCamera(update);
    }

    public void showCurrentLocation(MenuItem item) {
       Location currentLocation = LocationServices.FusedLocationApi
                .getLastLocation(mLocationClient);
        if (currentLocation == null) {
            Toast.makeText(this, "Couldn't connect!", Toast.LENGTH_SHORT).show();
        } else {
            LatLng latLng = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude() );
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.animateCamera(update);
        }
    }

    private void trackMe() {
    }

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this, "Ready to map!", Toast.LENGTH_SHORT).show();
        mListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Toast.makeText(MainActivity.this,
                        "Location changed: " + location.getLatitude() + ", " +
                                location.getLongitude(), Toast.LENGTH_SHORT).show();
            }
        };
        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(5000);
        request.setFastestInterval(1000);
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mLocationClient, request, mListener
        );
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mLocationClient, mListener
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        MapStateManager mgr = new MapStateManager(this);
        mgr.saveMapState(mMap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MapStateManager mgr = new MapStateManager(this);
        CameraPosition position = mgr.getSavedCameraPosition();
        if (position != null) {
            CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
            mMap.moveCamera(update);
//			This is part of the answer to the code challenge
            mMap.setMapType(mgr.getSavedMapType());
        }
    }

    private void removeEverything() {
        marker.remove();
        marker = null;
        shape.remove();
        shape = null;
    }

    private void setPoint() {
        Location currentLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mLocationClient);
            if (currentLocation == null)
            {
                Toast.makeText(this, "Couldn't connect! No merkers were set", Toast.LENGTH_SHORT).show();
            } else {
                LatLng latLngCurrent = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLngCurrent,15);
                mMap.animateCamera(update);
            }

        if (marker != null)
        {
            removeEverything();
        }
        //setting up point zero
        point0 = new LatLng(latitudeArr[0], longitudeArr[0]);
        MarkerOptions x0 = new MarkerOptions()
                .title("HOTSPOT0")
                .position(point0)
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_xspot));
        marker = mMap.addMarker(x0);
        circleOptions0 = new CircleOptions()
                .strokeWidth(3)
                .fillColor(0x330000FF)
                .strokeColor(Color.RED)
                .center(point0)
                .radius(2);
        shape = mMap.addCircle(circleOptions0);
        //setting up point 1
        point1 = new LatLng(latitudeArr[1], longitudeArr[1]);
        MarkerOptions x1 = new MarkerOptions()
                .title("HOTSPOT1")
                .position(point1)
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_xspot));
        marker = mMap.addMarker(x1);
        circleOptions1 = new CircleOptions()
                .strokeWidth(3)
                .fillColor(0x330000FF)
                .strokeColor(Color.RED)
                .center(point1)
                .radius(2);
        shape = mMap.addCircle(circleOptions1);
        //setting up point 2
        point2 = new LatLng(latitudeArr[2], longitudeArr[2]);
        MarkerOptions x2 = new MarkerOptions()
                .title("HOTSPOT2")
                .position(point2)
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_xspot));
        marker = mMap.addMarker(x2);
        circleOptions2 = new CircleOptions()
                .strokeWidth(3)
                .fillColor(0x330000FF)
                .strokeColor(Color.RED)
                .center(point2)
                .radius(2);
        shape = mMap.addCircle(circleOptions2);
    }

    public void infowin(){
        if (mMap != null) {
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }
                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.info_window, null);
                    TextView tvLocality = (TextView) v.findViewById(R.id.tvLocality);
                    TextView tvLat = (TextView) v.findViewById(R.id.tvLat);
                    TextView tvLng = (TextView) v.findViewById(R.id.tvLng);
                    TextView tvSnippet = (TextView) v.findViewById(R.id.tvSnippet);
                    //my current location vaviable
                    Location myCurrentLocation = LocationServices.FusedLocationApi
                            .getLastLocation(mLocationClient);

                    LatLng latLng = marker.getPosition();
                    //my marker location variable
                    Location markerLocation = new Location("marker_position");
                    markerLocation.setLatitude(latLng.latitude);
                    markerLocation.setLongitude(latLng.longitude);

                    float distancetoMarker = myCurrentLocation.distanceTo(markerLocation);

                    tvLocality.setText(marker.getTitle());
                    tvLat.setText("Latitude: " +latLng.latitude);
                    tvLng.setText("Longitude: " +latLng.longitude);
                    tvSnippet.setText("Distance: "+ distancetoMarker + "m");
                    return v;
                }
            });
        }
    }


    public void point() {
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            public void onMyLocationChange(Location location) {

                Location myLocation = new Location("point 0");
                myLocation.setLatitude(location.getLatitude());
                myLocation.setLongitude(location.getLongitude());

                Location locationA = new Location("point 0");
                locationA.setLatitude(latitudeArr[0]);
                locationA.setLongitude(longitudeArr[0]);

                Location locationB = new Location("point 1");
                locationB.setLatitude(latitudeArr[1]);
                locationB.setLongitude(longitudeArr[1]);

                Location locationC = new Location("point 2");
                locationC.setLatitude(latitudeArr[2]);
                locationC.setLongitude(longitudeArr[2]);

                distance0 = myLocation.distanceTo(locationA);
                distance1 = myLocation.distanceTo(locationB);
                distance2 = myLocation.distanceTo(locationC);

                Log.d("Testing multiple", distance0 + " " + distance1 + " " + distance2);

                smallestDistance = checkClosest(distance0, distance1, distance2);
                if (score != 3) {
                    if (smallestDistance == distance0 && !flag0 || (flag1 && distance0 < distance2) || (flag2 && distance0 < distance1)) {
                        flag1 = checkWin(distance0, point0);
                    } else if ((smallestDistance == distance1 && !flag1) || (flag0 && distance1 < distance2) || (flag2 && distance1 < distance0)) {
                        flag1 = checkWin(distance1, point1);
                    } else if ((smallestDistance == distance2 && !flag2) || (flag1 && distance1 > distance2) || (flag0 && distance2 > distance2)) {
                        flag2 = checkWin(distance2, point2);
                    }
                }
            }
        });
    }

    public float checkClosest(float distance0, float distance1, float distance2) {
        smallest = distance0;
        if (smallest > distance1) smallest = distance1;
        if (smallest > distance2) smallest = distance2;
        return smallest;
    }

    public boolean checkWin(float distance, LatLng point){
        if( distance > circleOptions0.getRadius() ){
            distanceFrom=distance;
        }
        if (distance < 10)
        {
            Toast.makeText(ctx, "You found one!", Toast.LENGTH_LONG).show();
            score++;
            speak();

            if(score == 3)
            {
                demotext = "Game Complete your the winner!";
                speak();
                Toast.makeText(ctx, "YOU WON!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }

    public static int TTS_DATA_CHECK = 1;
    public static int VOICE_RECOGNITION = 2;
    private TextToSpeech tts;
    private boolean ttsIsInit, ttsagainIsInit = false;
    public String demotext = "Congrats! X marks the spot";

    public void speak() {
        Intent intent = new Intent(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(intent, TTS_DATA_CHECK);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TTS_DATA_CHECK) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                Log.i("SpeechDemo", "## INFO 03: CHECK_VOICE_DATA_PASS");
                tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (tts.isLanguageAvailable(Locale.US) >= 0) {
                            tts.setLanguage(Locale.US);
                            tts.setPitch(0.8f);
                            tts.setSpeechRate(1.1f);
                            if (demotext.isEmpty()) {
                                Log.i("SpeechDemo", "## ERROR 02: Field is Empty");
                                demotext = "The field is empty. Type something first!";
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                ttsGreater21(demotext);
                            } else {
                                ttsUnder20(demotext);
                            }
                        }
                    }
                });
            } else {
                Log.i("SpeechDemo", "## INFO 04: CHECK_VOICE_DATA_FAILED, resultCode = " + resultCode);
                Intent installVoice = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installVoice);
            }
        } else if (requestCode == VOICE_RECOGNITION) {
            Log.i("SpeechDemo", "## INFO 02: RequestCode VOICE_RECOGNITION = " + requestCode);
            if (resultCode == RESULT_OK) {
                List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                float[] confidence = data.getFloatArrayExtra(RecognizerIntent.EXTRA_CONFIDENCE_SCORES);
                for (int i = 0; i < results.size(); i++) {
                    final String result = results.get(i);
                    Log.i("SpeechDemo", "## INFO 05: Result: " + result + " (confidence: " + confidence[i] + ")");
                }
            }
        } else {
        }
    }

    private void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void ttsGreater21(String text) {
        String utteranceId = this.hashCode() + "";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }

    public void testSpeechRec(View v) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        try {
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Just speak normally into your phone");
            intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
            startActivityForResult(intent, VOICE_RECOGNITION);
        } catch (ActivityNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void onClick_AddRecord(View v) {

        EditText title = (EditText) findViewById(R.id.title);
        Date cDate = new Date();
        String DATE = new SimpleDateFormat("yyyy-MM-dd").format(cDate);
        String passing_title = title.getText().toString();
        long newId = myDb.insertRow(passing_title, "discrition", DATE,"action","latitude","longitude","time",99.9);
        sendJSON sendJy = new sendJSON();
        sendJy.execute(passing_title, DATE);
        // Query for the record we just added.
        // Use the ID:
        Cursor cursor = myDb.getRow(newId);
        displayRecordSet(cursor);
    }

    public void onClick_ClearAll(View v) {
        myDb.deleteAll();
    }



}
