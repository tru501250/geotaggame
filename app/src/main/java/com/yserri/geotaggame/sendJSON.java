package com.yserri.geotaggame;
/**
 * Created by YserriCom4980 on 3/27/2016.
 */
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class sendJSON extends AsyncTask<String, String, String> {
    Context ctx;
    DateFormat df = new SimpleDateFormat("HH:mm:ss");
    double lat_ex = 50.667625d;
    double long_ex = -120.364295d;
    int score = 99;
    String strFileContents = null;
    String time_ex = this.df.format(Calendar.getInstance().getTime());

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings)
    {

        OutputStream os = null;
        InputStream is = null;
        HttpURLConnection conn = null;

        String Title_text = strings[0];
        String DESC_text = strings[1];
        double latt = Double.valueOf(strings[2]).doubleValue();
        double lont = Double.valueOf(strings[3]).doubleValue();
        String Action_text = strings[4];
        int score_val = Integer.valueOf(strings[5]).intValue();
        String TableID = strings[0];
        try {
            URL url = new URL("http://yserri.com/accessdb_r_.php");
            JSONObject jsonObject = new JSONObject();
            String DATE = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            jsonObject.put("TITLE", Title_text);
            jsonObject.put("DESC", DESC_text);
            jsonObject.put("DATE", DATE);
            jsonObject.put("ACTION", Action_text);
            jsonObject.put("Latitude", latt);
            jsonObject.put("Longitude", lont);
            jsonObject.put("Time", this.time_ex);
            jsonObject.put("Score", score_val);
            String message = jsonObject.toString();
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(1000);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            conn.connect();
            //setup send
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            //clean up
            os.flush();
            //do somehting with response
            is = conn.getInputStream();
            //String contentAsString = readIt(is,len);
            BufferedInputStream in = new BufferedInputStream(conn.getInputStream() );
            byte[] contents = new byte[1024];

            int bytesRead=0;

            while( (bytesRead = in.read(contents)) != -1){
                strFileContents = new String(contents, 0, bytesRead);
            }
            Log.d("DEBUG", "Input Stream = " + strFileContents);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            //clean up
            try {
                if ((os != null) && (is != null)) {
                    os.close();
                    is.close();
                }

                if (os == null) {

                    Log.d("DEBUG", " os NULL POINTERS");
                }

                if (is == null){
                    Log.d("DEBUG","is NULL POINTERS");

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {                       // Temp. Fix for Issue 2. Surround with Try/Catch to through an exception when URL-connection can't be closed
                conn.disconnect();
                if (strFileContents!=null){
                    //publishProgress("Data Sent to Server");
                    Log.d("DEBUG", "Data Sent to Server");
                }
            } catch (Exception e) {
                e.printStackTrace();
                //publishProgress("Server connection problem. Please Try again!");
                Log.d("DEBUG", "HttpURLConnection didn't work as expected");
            }

            return "";
        }

    } // End of doInBackground

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    @Override
    protected void onProgressUpdate(String... values) {

        super.onProgressUpdate(values);
        String msg = values[0];
    }

}
